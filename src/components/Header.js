import { MoonIcon, SunIcon } from "@heroicons/react/24/outline";

import React from "react";
import { useTheme } from "../contexts/ThemeContext";

const Header = () => {
  const { theme, toggleTheme } = useTheme();

  return (
    <>
      {theme === "light" ? (
        <header className="flex justify-between items-center p-4 bg-gray-100 ">
          <button
            onClick={toggleTheme}
            className="p-2 rounded-full bg-gray-300"
          >
            <MoonIcon className="h-6 w-6 text-black" />
          </button>
        </header>
      ) : (
        <header className="flex justify-between items-center p-4 bg-gray-900">
          <button
            onClick={toggleTheme}
            className="p-2 rounded-full bg-gray-700"
          >
            <SunIcon className="h-6 w-6 text-white" />
          </button>
        </header>
      )}
    </>
  );
};

export default Header;
