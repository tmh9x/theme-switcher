import React from "react";
import { useTheme } from "../contexts/ThemeContext";

const Main = () => {
  const { theme } = useTheme();
  return (
    <>
      {theme === "light" ? (
        <main className="flex-grow p-4 bg-gray-50 text-black">
          <h2 className="text-2xl ">Welcome to the Theme Switcher App</h2>
          <p className="mt-2">
            Toggle the theme using the button in the header.
          </p>
        </main>
      ) : (
        <main className="flex-grow p-4 bg-gray-800 text-white">
          <h2 className="text-2xl ">Welcome to the Theme Switcher App</h2>
          <p className="mt-2">
            Toggle the theme using the button in the header.
          </p>
        </main>
      )}
    </>
  );
};

export default Main;
