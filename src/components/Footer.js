import React from "react";
import { useTheme } from "../contexts/ThemeContext";

const Footer = () => {
  const { theme } = useTheme();
  return (
    <>
      {theme === "light" ? (
        <footer className="fixed bottom-0 w-full p-4 bg-gray-100 text-center text-black">
          <p>© 2024 Your Company</p>
        </footer>
      ) : (
        <footer className="fixed bottom-0 w-full p-4 dark:bg-gray-900 text-center text-white">
          <p>© 2024 Your Company</p>
        </footer>
      )}
    </>
  );
};

export default Footer;
